<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181231145421 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_4C62E638A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact AS SELECT id, user_id, first_name, last_name, title, street, house_no, zip, city, country, phone, mobile, dob, email, picture, is_deleted, created_at, updated_at, tags FROM contact');
        $this->addSql('DROP TABLE contact');
        $this->addSql('CREATE TABLE contact (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, first_name VARCHAR(100) NOT NULL COLLATE BINARY, last_name VARCHAR(100) NOT NULL COLLATE BINARY, title VARCHAR(10) DEFAULT \'Mr.\' NOT NULL COLLATE BINARY, street VARCHAR(100) NOT NULL COLLATE BINARY, house_no VARCHAR(30) NOT NULL COLLATE BINARY, zip VARCHAR(10) NOT NULL COLLATE BINARY, city VARCHAR(100) NOT NULL COLLATE BINARY, country VARCHAR(100) NOT NULL COLLATE BINARY, phone VARCHAR(20) NOT NULL COLLATE BINARY, mobile VARCHAR(20) DEFAULT NULL COLLATE BINARY, dob DATE DEFAULT NULL, email VARCHAR(254) NOT NULL COLLATE BINARY, picture VARCHAR(255) DEFAULT NULL COLLATE BINARY, is_deleted BOOLEAN DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME NOT NULL, tags VARCHAR(100) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_4C62E638A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO contact (id, user_id, first_name, last_name, title, street, house_no, zip, city, country, phone, mobile, dob, email, picture, is_deleted, created_at, updated_at, tags) SELECT id, user_id, first_name, last_name, title, street, house_no, zip, city, country, phone, mobile, dob, email, picture, is_deleted, created_at, updated_at, tags FROM __temp__contact');
        $this->addSql('DROP TABLE __temp__contact');
        $this->addSql('CREATE INDEX IDX_4C62E638A76ED395 ON contact (user_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D6497CCD6FB7');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, preferences_id, username, password, email, full_name, roles, confirmation_token, enabled FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, preferences_id INTEGER DEFAULT NULL, username VARCHAR(50) NOT NULL COLLATE BINARY, password VARCHAR(255) NOT NULL COLLATE BINARY, email VARCHAR(254) NOT NULL COLLATE BINARY, full_name VARCHAR(50) NOT NULL COLLATE BINARY, roles CLOB DEFAULT \'ROLE_USER\' NOT NULL COLLATE BINARY --(DC2Type:simple_array)
        , confirmation_token VARCHAR(30) DEFAULT NULL COLLATE BINARY, enabled BOOLEAN NOT NULL, CONSTRAINT FK_8D93D6497CCD6FB7 FOREIGN KEY (preferences_id) REFERENCES user_preferences (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO user (id, preferences_id, username, password, email, full_name, roles, confirmation_token, enabled) SELECT id, preferences_id, username, password, email, full_name, roles, confirmation_token, enabled FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497CCD6FB7 ON user (preferences_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_4C62E638A76ED395');
        $this->addSql('CREATE TEMPORARY TABLE __temp__contact AS SELECT id, user_id, first_name, last_name, title, street, house_no, zip, city, country, phone, mobile, dob, email, picture, is_deleted, created_at, updated_at, tags FROM contact');
        $this->addSql('DROP TABLE contact');
        $this->addSql('CREATE TABLE contact (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, user_id INTEGER NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, title VARCHAR(10) DEFAULT \'Mr.\' NOT NULL, street VARCHAR(100) NOT NULL, house_no VARCHAR(30) NOT NULL, zip VARCHAR(10) NOT NULL, city VARCHAR(100) NOT NULL, country VARCHAR(100) NOT NULL, phone VARCHAR(20) NOT NULL, mobile VARCHAR(20) DEFAULT NULL, dob DATE DEFAULT NULL, email VARCHAR(254) NOT NULL, picture VARCHAR(255) DEFAULT NULL, is_deleted BOOLEAN DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME NOT NULL, tags VARCHAR(100) DEFAULT NULL)');
        $this->addSql('INSERT INTO contact (id, user_id, first_name, last_name, title, street, house_no, zip, city, country, phone, mobile, dob, email, picture, is_deleted, created_at, updated_at, tags) SELECT id, user_id, first_name, last_name, title, street, house_no, zip, city, country, phone, mobile, dob, email, picture, is_deleted, created_at, updated_at, tags FROM __temp__contact');
        $this->addSql('DROP TABLE __temp__contact');
        $this->addSql('CREATE INDEX IDX_4C62E638A76ED395 ON contact (user_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('DROP INDEX UNIQ_8D93D6497CCD6FB7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, preferences_id, username, password, email, full_name, roles, confirmation_token, enabled FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, preferences_id INTEGER DEFAULT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(254) NOT NULL, full_name VARCHAR(50) NOT NULL, roles CLOB DEFAULT \'ROLE_USER\' NOT NULL --(DC2Type:simple_array)
        , confirmation_token VARCHAR(30) DEFAULT NULL, enabled BOOLEAN NOT NULL)');
        $this->addSql('INSERT INTO user (id, preferences_id, username, password, email, full_name, roles, confirmation_token, enabled) SELECT id, preferences_id, username, password, email, full_name, roles, confirmation_token, enabled FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497CCD6FB7 ON user (preferences_id)');
    }
}
