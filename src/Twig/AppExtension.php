<?php
namespace App\Twig;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function __construct()
    {
    }

    public function getFilters()
    {
        return [
            new TwigFilter('modlink', [$this, 'modifyURL']) ,
        ];
    }

    public function modifyURL($url = '', $var = '', $val = '')
    {
        $url_parts = explode("?", $url);
        if (count($url_parts) > 1)
        {
            $params = explode("&", $url_parts[1]);
            for ($i = 0;$i < count($params);$i++)
            {
                list($_var, $_val) = explode("=", $params[$i]);
                if ($_var == $var)
                {
                    unset($params[$i]);
                }
            }
            $params[] = "$var=$val";
            return $url_parts[0] . "?" . implode("&", $params);
        }
        else
        {
            return $url . "?$var=$val";
        }
        return $url;
    }

}

