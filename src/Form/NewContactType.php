<?php
namespace App\Form;
use App\Entity\Contact;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TelType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NewContactType extends AbstractType{

	public function buildForm(FormBuilderInterface $builder, array $options){
		$builder
		->add('firstName',TextType::class)
		->add('lastName',TextType::class)
		->add('street',TextType::class)
		->add('houseNo',TextType::class)
		->add('zip',TextType::class)
		->add('city',TextType::class)
		->add('country',CountryType::class, ['data' => 'DE'])
		->add('phone',TelType::class, ['required'=> false])
		->add('mobile',TelType::class, ['required'=> false])
		->add('email',EmailType::class, ['required'=> false])


		->add('save', SubmitType::class);
	}

	public function configureOptions(OptionsResolver $resolver){
		$resolver->setDefaults([
			'data_class' => Contact::class
		]);
	}
}