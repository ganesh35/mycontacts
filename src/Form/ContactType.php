<?php
namespace App\Form;
use App\Entity\Contact;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TelType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;




class ContactType extends AbstractType{

	public function buildForm(FormBuilderInterface $builder, array $options){
		$builder
		->add('title', ChoiceType::class, [
			'choices' => [
				'Mr.' => 'Mr.',
				'Mrs.' => 'Mrs.',
				'Dr.' => 'Dr.',
				'Prof.' => 'Prof.',
				'Miss.' => 'Miss.'
			]
		])
		->add('firstName',TextType::class)
		->add('lastName',TextType::class)
		->add('street',TextType::class)
		->add('houseNo',TextType::class)
		->add('zip',TextType::class)
		->add('city',TextType::class)
		->add('country',CountryType::class, ['empty_data' => 'de'])

		->add('phone',TelType::class, ['required'=> false])
		->add('mobile',TelType::class, ['required'=> false])
		->add('email',EmailType::class, ['required'=> false])
		->add('dob', DateType::class, ['widget' => 'single_text', 'label' => 'Date of Birth', 'required'=> false])
		->add('picture', FileType::class, ['label' => 'Profile picture', 'required'=> false, 'data_class' => null])
		->add('tags',TextType::class, ['label' => 'Tags', 'required'=> false])
		->add('save', SubmitType::class);
	}

	public function configureOptions(OptionsResolver $resolver){
		$resolver->setDefaults([
			'data_class' => Contact::class
		]);
	}
}