<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class DashboardController extends Controller{

	/**
	* @Route("/dashboard", name="dashboard_index")
	* @Security("is_granted('ROLE_USER')")
	*/
	public function index(Request $request){
		return $this->render('dashboard/menu.html.twig',[]);
	}

}


