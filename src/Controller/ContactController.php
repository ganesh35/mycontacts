<?php
namespace App\Controller;
use App\Entity\User;
use App\Entity\Contact;
use App\Repository\ContactRepository;
use App\Form\ContactType;
use App\Form\NewContactType;

use App\Lib\Paginator;


//use Symfony\Component\Form\FormFactoryInterface;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

	/**
	* @Route("/contact")
	*/
class ContactController extends Controller{

	/**
	* @var ContactRepository
	*/
	private $contactRepository;

	/**
	* @var entityManager
	*/
	private $entityManager;

	public function __construct(
		ContactRepository $contactRepository,
		EntityManagerInterface $entityManager
	){
		$this->contactRepository = $contactRepository;
		$this->entityManager = $entityManager;
	}

	/**
	* @Route("/list", name="contact_index")
	* @Security("is_granted('ROLE_USER')")
	*/
	public function index(Request $request, TokenStorageInterface $tokenStorage){
		$currentUser = $tokenStorage->getToken()->getUser();
		// sort
		$sort = [];
		$sortby = $request->query->get('sortby', 'createdAt');
		$orderby = strtoupper($request->query->get('orderby', 'DESC') ); 
		$sort[$sortby] = $orderby;

		// search
		$where = [];
		$search_key = $request->query->get('search_key', 'tags');
		$search_val = $request->query->get('search_val');
		if(!empty($search_key) && !empty($search_val)){
			$where[$search_key] = $search_val;
		}

		// Filter by user but not for ADMIN
		if ($currentUser instanceof User) {
			if( !in_array('ROLE_ADMIN', $currentUser->getRoles()) ){
				$where['user'] = $currentUser;
			}
		}

		$paginator = new Paginator($request, $this->container);
		list($count, $res)   = $this->contactRepository->getResultAndCount( $where, $sort, $paginator->getRecordsPerPage(), $paginator->getCurrentPage() );
		$paginator->setTotalPages($count);

		return $this->render('contact/list.html.twig',[
			'contacts' => $res,
			'paginator' => $paginator, 
			'sortFields' => ['firstName' => 'First name', 'lastName' => 'Last name', 'email' => 'Email' , 'mobile' => 'Mobile', 'createdAt' => 'Date created', 'updatedAt' => 'Date updated' ],
			'searchFields' => ['firstName' => 'First name', 'lastName' => 'Last name', 'email' => 'Email' , 'mobile' => 'Mobile', 'tags' => 'Tags'],
			'sortby' => $sortby,
			'orderby' => $orderby,
			'search_key' => $search_key,
			'search_val' => $search_val
		]);
	}

	/**
	* @Route("/add", name="contact_add")
	* @Security("is_granted('ROLE_USER')")
	*/
	public function add(Request $request, TokenStorageInterface $tokenStorage){

		$contact = new Contact();
		//$contact->setCreatedAt(new \DateTime());   // moved to prepersist
		$contact->setTitle('Mr.');
		$contact->setUpdatedAt(new \DateTime());
		
		$user = $tokenStorage->getToken()->getUser();
		$contact->setUser($user);

		$form = $this->createForm(
			NewContactType::class,
			$contact
		);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid() ){
            $this->entityManager->persist($contact);
            $this->entityManager->flush();
            $this->addFlash("success", "Record created successfully. ". $contact->getEmail()." (".$contact->getId().")" );
            return $this->redirectToRoute('contact_index');
		} else {
			return $this->render('contact/add.html.twig',[
				'form' => $form->createView(),
			]);	
		}

		
	}

	/**
	* @Route("/edit/{id}", name="contact_edit")
	* @Security("is_granted('edit', contact)", message="Access denied")
	*/
	public function edit(Contact $contact, Request $request){
		$form = $this->createForm( ContactType::class, $contact );
		
		// ToDo: Custom validation for the Phone number based on Country 

		$form->handleRequest($request);
		if($form->isSubmitted() && $form->isValid() ){

			/**
			* @var UploadedFile $file
			*/
			$file = $contact->getPicture();

			$fileName = '';
			if($file){
				$fileName = md5(uniqid()).'.'.$file->guessExtension();
				// Move the file to the directory where images are stored
	            try {
	                $file->move( $this->container->getParameter('image_folder'), $fileName);
	            } catch (FileException $e) {
	                // ... handle exception if something happens during file upload
	            }
            }
            
            $contact->setPicture($fileName);

            $this->entityManager->flush();
            $this->addFlash("success", "Record updated successfully.  ". $contact->getTitle().' '.$contact->getLastName());
            return $this->redirectToRoute('contact_index');
		}
		return $this->render('contact/edit.html.twig',[
			'form' => $form->createView(),
			'contact' => $contact
		]);

	}

	/**
	* @Route("/delete/{id}", name="contact_delete")
	* @Security("is_granted('delete', contact)", message="Access denied")
	*/
	public function delete(Contact $contact){
		$this->addFlash("success", "Record deleted successfully.  ". $contact->getEmail()." (".$contact->getId().")");
		$this->entityManager->remove($contact);
		$this->entityManager->flush();
		return $this->redirectToRoute('contact_index');
	}		

	/**
	* @Route("/confirm/{id}", name="contact_confirm")
	*/
	public function confirmAction(Contact $myobject) {
	    // check my object exist
	    if (!$myobject) {
	        $this->addFlash("warn", "Record not found". $myobject->getId());
	    } else {
	        // you can pass information about your object to the confirmation message
	        $myobjectInfo = array(
	            'yes' => 'contact_delete', // path to the deleteAction, required
	            'no' => 'contact_index', // path if cancel delete, required
	            // put any information here. I used type, name and id
	            // but you can add what you want
	            'type' => 'delete', 
	            'id' => $myobject->getId(), // required for deleteAction path
	            'name' => $myobject->getEmail()
	        );
	        // add informations to session variable
	        $this->get('session')->set('confirmation',$myobjectInfo);
	        return $this->render('tools/dialog.html.twig',[]);
	    }
	}


}
