<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use App\Entity\User;
use App\Entity\UserPreferences;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    private const USERS = [
        [
            'username' => 'stefan',
            'email' => 'stefan@stefan.com',
            'password' => 'stefan*1*2*3*',
            'fullName' => 'Stefan Benzberg',
            'roles' => [User::ROLE_USER]
        ],
        [
            'username' => 'ganesh',
            'email' => 'ganesh35@gmail.com',
            'password' => 'ganesh*1*2*3*',
            'fullName' => 'Ganesh K. Butcha',
            'roles' => [User::ROLE_USER]
        ],
        [
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => 'admin*1*2*3*',
            'fullName' => 'Dev Admin',
            'roles' => [User::ROLE_ADMIN]
        ],
    ];


    private const LANGUAGES = [
        'en',
        'de'
    ];

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    { 
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    { 
        $this->loadUsers($manager);
        $this->loadContacts($manager); 
    }

    private function loadContacts(ObjectManager $manager)
    { 
        for ($i = 0; $i < 50; $i++) {     
            $c = new Contact();     
            $c->setTitle($this->fakeTitle());
            $c->setFirstName($this->fakeFirstName());
            $c->setLastName($this->fakeLastName());
            $c->setEmail(
                strtolower(
                    $c->getFirstName()."@".$c->getLastName().'.'.substr($c->getTitle(), 0, 2)  
                )
            );
            $c->setMobile('157'.rand (  1000, 9999).rand (  1000, 9999));
            $c->setPhone('157'.rand (  1000, 9999).rand (  1000, 9999));
            $c->setCountry($this->fakeCountry());
            $c->setCity($this->fakeCity());
            $c->setZip(rand ( 10101 , 89000 ));
            $c->setStreet($this->fakeLastName().' '.$this->fakeCity());
            $c->setHouseNo(rand ( 10, 999));

            $c->setUser($this->getReference(
                self::USERS[rand(0, count(self::USERS) - 1)]['username']
            ));

            $date = new \DateTime();
            $date->modify('-' . rand(30, 50) . ' day');
            $c->setCreatedAt($date);

            $date = new \DateTime();
            $date->modify('-' . rand(1, 25) . ' day');

            $c->setUpdatedAt($date);
            $c->setTags('test, data');
            $manager->persist($c); 
        }
        $manager->flush();
    }

    private function loadUsers(ObjectManager $manager)
    {
        foreach (self::USERS as $userData) {
            $user = new User();
            $user->setUsername($userData['username']);
            $user->setFullName($userData['fullName']);
            $user->setEmail($userData['email']);
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $userData['password']
                )
            );
            $user->setRoles($userData['roles']);
            $user->setEnabled(true);

            $this->addReference(
                $userData['username'],
                $user
            );

            $preferences = new UserPreferences();
            $preferences->setLocale(self::LANGUAGES[rand(0, 1)]);

            $user->setPreferences($preferences);
            $manager->persist($user);
        }

        $manager->flush();
    }



    private function fakeTitle(){ 
        $array = ['Mr.', 'Mrs.', 'Dr.', 'Prof.', 'Miss.', 'Master.'];
        return $array[rand ( 0 , count($array) -1)];
    }
    private function fakeFirstName(){ 
        $array = ['Johnathon', 'Anthony', 'Erasmo', 'Raleigh', 'Nancie', 'Tama', 'Camellia', 'Augustine', 'Christeen', 'Luz', 'Diego', 'Lyndia', 'Thomas', 'Georgianna', 'Leigha', 'Alejandro', 'Marquis', 'Joan', 'Stephania', 'Elroy', 'Zonia', 'Buffy', 'Sharie', 'Blythe', 'Gaylene', 'Elida', 'Randy', 'Margarete', 'Margarett', 'Dion', 'Tomi', 'Arden', 'Clora', 'Laine', 'Becki', 'Margherita', 'Bong', 'Jeanice', 'Qiana', 'Lawanda', 'Rebecka', 'Maribel', 'Tami', 'Yuri', 'Michele', 'Rubi', 'Larisa', 'Lloyd', 'Tyisha', 'Samatha'];
        return $array[rand ( 0 , count($array) -1)];
    }
    private function fakeLastName() {
        $array = [ 'Mischke', 'Serna', 'Pingree', 'Mcnaught', 'Pepper', 'Schildgen', 'Mongold', 'Wrona', 'Geddes', 'Lanz', 'Fetzer', 'Schroeder', 'Block', 'Mayoral', 'Fleishman', 'Roberie', 'Latson', 'Lupo', 'Motsinger', 'Drews', 'Coby', 'Redner', 'Culton', 'Howe', 'Stoval', 'Michaud', 'Mote', 'Menjivar', 'Wiers', 'Paris', 'Grisby', 'Noren', 'Damron', 'Kazmierczak', 'Haslett', 'Guillemette', 'Buresh', 'Center', 'Kucera', 'Catt', 'Badon', 'Grumbles', 'Antes', 'Byron', 'Volkman', 'Klemp', 'Pekar', 'Pecora', 'Schewe', 'Ramage'];
        return $array[rand ( 0 , count($array) -1)];
    }
    private function fakeCity(){ 
        $array = ["Vuodburg", "Xegas", "Ylourlens", "Mipolis", "Luvale", "Xeley", "Vaka", "Lando", "Andville", "Isonstin", "Ipraading", "Neson", "Napolis", "Drimnard", "Praaridge", "Srago", "Vrathe", "Prock", "Illephia", "Enscaster", "Yeemond", "Droding", "Qaeby", "Clunburgh", "Fobert", "Rosa", "Cin", "Zlark", "Orepool", "Agomery", "Srisson", "Siford", "Vinmont", "Frapool", "Blamond", "Vleah", "Khery", "Wrago", "Ontledo", "Eighling"];
        return $array[rand ( 0 , count($array) -1)];
    }
    private function fakeCountry(){ 
        $array = ["AZ", "DE", "FR", "AU", "BG", "RU", "DK", "IN", "US", "NL", "SW"];
        return $array[rand ( 0 , count($array) -1)];
    }


}
