<?php

namespace App\Security;

use App\Entity\Contact;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContactVoter extends Voter
{
    const EDIT = 'edit';
    const DELETE = 'delete';
    const ADD = 'add';
    const LIST = 'list';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT, self::DELETE, self::ADD, self::LIST])) {
            return false;
        }

        if (!$subject instanceof Contact) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(
        $attribute, $subject, TokenInterface $token
    ) {
        if ($this->decisionManager->decide($token, [User::ROLE_ADMIN])) {
            return true;
        }

        $authenticatedUser = $token->getUser();

        if (!$authenticatedUser instanceof User) {
            return false;
        }

        /** @var Contact $contact */
        $contact = $subject;

        return $contact->getUser()->getId() === $authenticatedUser->getId();
    }
}