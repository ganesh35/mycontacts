# myContacts

Manage contacts at one place.


## Install packages
Installing required packages and dependencies.
```sh
composer install
php security-checker.phar security:check composer.lock
```

## Save changes to db
Create database schema
```sh
php bin/console doctrine:schema:update --force
```

## Loading Fixtures
Auto generating test data on the database.

```sh
php bin/console doctrine:fixtures:load
```

## Configure .env
All sensitive information related to server goes here. 

```sh
mv .env.prod .env
```

## Start and serve pages 
Run if it is a local installation
```sh
php bin/console server:run
```